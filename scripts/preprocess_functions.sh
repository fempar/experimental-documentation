#!/bin/bash

abs_path=$(dirname $(readlink -f $0))

#####################################################################
# Create a new page given its path, title, author and content.
#####################################################################
function write_page() {
    # Check number of arguments
    if [ "$#" -ne 4 ]; then
        echo "[ERROR] Wrong number of arguments: 4 expected" >&2
        echo "">&2
        echo "Usage: write_page PATH TITLE AUTHOR CONTENT" >&2
        echo "">&2
        echo "Input parameters:">&2
        echo "  # \$1 : PATH (Path to page file)">&2
        echo "  # \$2 : TITLE (Title of the generaged page)">&2
        echo "  # \$3 : AUTHOR (Author of the generaged page)">&2
        echo "  # \$4 : CONTENT (Content of the generated page)">&2
        exit 1
    fi
    local path="$1" title="$2" author="$3" content=$4;
    # Check base directory
    local base_path=$(dirname $path)
    if [ ! -d $base_path ]; then
        mkdir -p $base_path
        if [ $? -ne 0 ]; then 
            echo "[ERROR] Directory: $base_path" >&2
            exit 1
        fi
    fi
    # Write page file
    echo "  Writing page: $path"
cat << EOF > $path
Title: $title
Author: $author

$content

EOF
}

#####################################################################
# Search for files matching a regex and create a set of markdown pages
# including the original ones.
# Pages must contain, at least, the `title` attribute to be managed by
# the authomatic html documentation generator.
#####################################################################
function create_reference_pages_recursive() {
    # Check number of arguments
    if [ "$#" -ne 5 ]; then
        echo "[ERROR] Wrong number of arguments: 5 expected" >&2
        echo "">&2
        echo "Usage: include_pages_recursive ORIGIN DESTINY REGEX DEPTH TITLE" >&2
        echo "">&2
        echo "Input parameters:">&2
        echo "  # \$1 : ORIGIN (Root path to start searching for files)">&2
        echo "  # \$2 : DESTINY (Root path to create pages including found files)">&2
        echo "  # \$3 : REGEX (Reges to be used by find command to search for files)">&2
        echo "  # \$4 : DEPTH (Recursion depth)">&2
        echo "  # \$5 : TITLE (Title of the generated pages section)">&2
        exit 1
    fi
    local origin="$1" destiny="$2" regex=$3 depth="$4" title="$5";

    echo "Searching $regex files in $origin (depth: $depth)"

    # Check if main index page exists in destiny
    local index_page=$destiny/index.md
    if [ ! -f $index_page ]; then
        write_page "$index_page" "$title" "Large Scale Scientific Computing" "## [$title] Documented components:"
    fi

    # Find and count markdown files
    local files_list=$(cd $origin && find . -maxdepth $depth -type f -name $regex -printf "%P\n" |  tr '\n' '#')
    local num_files=$( echo $files_list | tr -cd '#' | wc -c )

    # Reference origin markdown files from destiny
    # Warning! It expects only one markdown file per directory level
    local file_id=1
    while [ $file_id -le $num_files ]
    do 
        local file_path=$(echo $files_list | cut -d# -f$file_id)
        local file_dir=$(dirname $file_path)
        local file_name=$( basename $file_dir )
        echo "  Found: $file_path. Referenced from: $destiny/$file_name.md"
        write_page "$destiny/$file_name.md " "$file_name" "Large Scale Scientific Computing" "{!$origin/$file_path!}"
        # Sff 
        echo " *  [$file_name]($file_name.html)" >> $index_page
        let file_id=file_id+1
    done

}

#####################################################################
# Search for tutorials subdirs given a origin root directory
# in order to generage tutorial pages from the commented source code
#   - Tutorial sources must be located under the ORIGIN directory.
#   - Each tutorial is contained in its own subdirectory one level under ORIGIN
#####################################################################
function create_tutorial_pages_from_source() {

    # Check number of arguments
    if [ "$#" -ne 2 ]; then
        echo "[ERROR] Wrong number of arguments: 2 expected" >&2
        echo "">&2
        echo "Usage: include_pages_recursive ORIGIN DESTINY" >&2
        echo "">&2
        echo "Input parameters:">&2
        echo "  # \$1 : ORIGIN (Path to FEMPAR tutorials root directory)">&2
        echo "  # \$2 : DESTINY (Path to Tutorial pages root directory)">&2
        exit 1
    fi
    local origin="$1" destiny="$2";

    local index_page=$destiny/index.md
    if [ ! -f $index_page ]; then
        write_page "$index_page" "Tutorials" "Large Scale Scientific Computing" "## [Tutorials] Documented components:"
    fi


    # List and count directories under tutorials dir
    tutorial_list=$(ls -d  $origin/*/ | tr '\n' '#')
    num_tutorials=$( echo $tutorial_list |  sed 's/#/\n/g' | wc -l )

    # Iterate to generate Markdown documentation per tutorial
    tutorial_id=1
    while [ $tutorial_id -le $num_tutorials ]
    do 
        tutorial_path=$(echo $tutorial_list | cut -d# -f$tutorial_id)
        if [ ! $tutorial_path == '' ]
        then
            tutorial_name=$( basename $tutorial_path )
            $abs_path/generate_tutorial_page.sh $origin $destiny $tmp_data_dir $tutorial_name 
            echo " *  [$tutorial_name]($tutorial_name/index.html)" >> $index_page
        fi
        let tutorial_id=tutorial_id+1
    done

}
