Title: Writing documentation
Date: 25/10/2018
Author: Pere Antoni Martorell
Email: pmartorell@cimne.upc.edu


[TOC]


## What FORD is?

According to [FORD wiki](https://github.com/Fortran-FOSS-Programmers/ford/wiki) by [cmacmackin](https://github.com/cmacmackin):
>FORD is an automatic documentation generator for modern Fortran programs. It stands for FORtran Documenter. 
>As you may know, "to ford" refers to crossing a river (or other body of water). 
>It does not, in this context, refer to any company or individual associated with cars.

For those who know [Doxygen](http://www.doxygen.org/), FORD aims to achieve similar documentation features in Fortran.

## How to intall FORD
 These pages are supported by FORD 6.0.0, which can be installed using *pip* as in the [installation page](https://github.com/Fortran-FOSS-Programmers/ford/wiki/Installation) of the FORD wiki. 

So that, simply, install *pip*:
```
$ sudo apt-get install python-pip python-dev build-essential
```
Install FORD:
```
$ sudo pip install ford
```
And check the installed version with
```
$ ford -V
```
Additionally, for the preprocessing scripts [Pandoc](https://pandoc.org/installing.html) is needed, as well as access to the `articles` GitLab repository (located in the same root directory than FEMPAR).
## How to use FORD in FEMPAR

### How to run FORD in FEMPAR
Place in FEMPAR source directory, in a branch which contains commit `e18c9c9c`, then create the documentation with the preconfigured bash script

```
$ $FEMPAR_DIR/Tools/DocScripts/create_doc.sh
```
where `$FEMPAR_DIR` is the path to the FEMPAR directory.

The output will be placed on `./html/`, thus run  
```
$ firefox html/index.html
```
to see the generated documentation.

@note
The directories of analysis can be modified in
    ```
    Tools/DocScripts/data/ford_project_file.md
    ```


### Documentation style
In FORD, the comments with certain *docmarks* are parsed in the documentation (more information in [documentation rules](https://github.com/Fortran-FOSS-Programmers/ford/wiki/Writing-Documentation)), in FEMPAR those  *docmarks* are choosen to be the following:

 - **Predocmark:** Is `!>` in FEMPAR, it introduces documentation before the type, function or variable is defined.
 - **Docmark:** Is `!<` in FEMPR, it introduces documentation after the type function or variable is defined.  

Here an example of how those *docmarks* are used:

```fortran

!> summary: Get DOF vaules at next timestep by solving a system in each RK stage
!> The function `apply` does the following:  
!>


subroutine dirk_solver_apply( this, x, y )
  implicit none
  class(dirk_solver_t),   intent(inout) :: this
  class(vector_t)     ,   intent(in)    :: x !< Input DOF values
  class(vector_t)     ,   intent(inout) :: y !< Output DOF values
  integer(ip) :: i
  type(time_stepping_stage_fe_operator_t), pointer :: stage_op

  assert ( associated(this%ts_op) )
  assert ( associated(this%nl_solver) )
 
!<  - Set `x` as initial data, i.e., \(u_h^0=x\)  
!<  - For each RK stage (i)  
!<    - Interpolate BC at \(t_i = t + c_i \Delta t \), \(u_h^{\varphi} (t_i) \)  and \(\partial u_h^{\varphi} (t_i) \)   
!<    - Set discrite integration time at \(t_i\)
!<    - Integrate mass matrix ( and affine operator if lineal )   
!<    - Solve a non-linear system for each `dof_stages(i)` (\(y_i\)), with:
!<        $$ R_{ii} (y) = My + M\partial_t u_h^{\varphi} (t + c_i \Delta t)  + \bar A (  t + c_i \Delta t, X_i (Y_{|1:i-1|},y),$$
!<        $$ \frac{\partial R_{ii} (y)} {\partial y} = M + a_{ii}\Delta t \frac{\partial \bar A (t + c_i \Delta t,x)}{\partial x}$$
!<  - Set output `y` as \(u_h^1 = u_h^0 + \Delta t \sum_{i=1}^s b_i y_i \)
 
  call this%ts_op%set_initial_data(x) 
  call y%copy(x)
  do i = 1, this%ts_op%scheme%num_stages
    stage_op => this%ts_op%get_stage_operator(i,i)

    call stage_op%set_evaluation_time( this%get_current_time() + this%get_time_step_size() * this%ts_op%scheme%c(i) )
    call this%nl_solver%apply ( this%rhs , this%ts_op%dofs_stages(i) )
    call y%axpby(this%get_time_step_size()*this%ts_op%scheme%b(i),this%ts_op%dofs_stages(i),1.0_rp)
  end do
end subroutine dirk_solver_apply
```

Where we can highlight the following elements:

* Summary (optional): By default the first paragraph of each element, e.g. module, type, subroutine ..., it appeareas as a short description when it is listed. 
* Header documentation: Complete description of each element, e.g. module, type, subroutine ..., it appears on its own descpiption page ( see [output](../proc/dirk_solver_apply.html)).
* Variable descriptor: Side descriptor on the member variable table.
* Inside documentation: All the documentation inside each element will be attached to the header documentation on the description page ( see another [example](../proc/dirk_solver_advance_fe_function.html)).

@note
Within the documentation comments, i.e., started by a *dockmark*, markdown is supported with special equations format:
In text equations `\( ... \)` , new line equations `$$ ... $$` and `\[ ... ]\` and enumerated equations `\begin{equation} ... \end{equation}`
@endnote

### Tutorials
The tutorials placed in the folder `Tutorials` are processed as continuous text documents as is done in [deal.ii](https://www.dealii.org/developer/doxygen/deal.II/Tutorial.html) with [Doxygen](http://www.doxygen.org/). 

Several ad-hoc scripts has been created to perform this task. So, to follow this instructions is extreamelly important to make automatic tutorials documentation work.

The documentation of the tutorials is extracted from the source code with the following simple rules:

 * All lines started with `!*` are parsed as documentation expecting to be in Markdown with a continuous plot
 * All other commented lines started with `!` are ignored in this documentation, even lines begining with `!**`
 * The rest of the code is encapsulated in syntax higlighted code boxes

@note
The comments at the end of the line are not ignored, hence those are highligthted as comments in the code boxes
@endnote

 
### LaTEX and MarkDown reports
In order to include reports and documents in the documentation. The following syntax can be used in the any section of the documentation: the MarkDown report name in between: \{! ... !\}, e.g., \{!`fempar/README.md`!\}.

LaTEX report can be translated into MarkDown using [Pandoc](https://pandoc.org/installing.html), once Pandoc is installed, an script (`fortran2markdown.sh`) contains the requirements of MarkDown for FORD:
```
$ $FEMPAR_DIR/Tools/DocScripts/fortran2markdow.sh -h
```
Then place the resulting MarkDown (`.md`) report into `Documentation/Reports` to be used as an include.
@note
**Pandoc** has some limitations with respect to some LaTEX packages and features. Therefore, revision and manual **post-processing** of the MarkDown files is highly recommendable.
@endnote

## Tasks in progress
As the the developement of these tools still in process, there are some tasks todo and other pending:

 * Miscelaneous detailed task in [ToDo page](todo.html)
 * `experimental` merge.
 * Periodic automatic generation of the documentation in server, .e.g., *serverconfus*.


## Feedback appreciated

Please, do not hesitate to provide feedback about the state of this tool. 

In the following FEMPAR GitLab issue [#262](https://gitlab.com/fempar/fempar/issues/262) you can write comments and discussions with missing features, limitations, etc. and possible solutions for these.



