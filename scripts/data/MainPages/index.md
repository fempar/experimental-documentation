Title: Documentation

## The FORD tool in FEMPAR
The FEMPAR is documented by using [**FORD**](http://fortranwiki.org/fortran/show/FORD). However, for the purposes of FEMPAR documentation, FORD presents some limitations which are by-passed through customized tools, i.e., this page section have been enriched with updated source code.

More information in [How to document](presentation.html). 


## Documentation elements
This documentation of FEMPAR contain the following elements:

 * [Home Page](../index.html) with general information
 * [Documentation Section](index.html), current page; with additional information.
    - [Containers](02_containers/index.html): How to use **FEMPAR** withing containers.
    - [Tutorials](03_tutorials/index.html): Tutorials, documented with a continued plot.
    - [Core](04_core/index.html): Documentation of **FEMPAR** core components
    - [Thirdparty](05_thirdparty/index.html): Documentation of **FEMPAR** thirdparty components
    - [ToDo](todo.html): List of future developments.
    - [Writing documentation](writing_documentation.html): How to contribute to **FEMPAR** documentation.
 * **FORD core Documentation**: Lists, descriptions and dependencies of every Program, Module, Procedure and Data Type in FEMPAR.


