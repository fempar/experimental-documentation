Title: ToDo
Author: Large Scale Scientific Computing

## Improvements
@todo
Merge repeated procedures with `~2` in `.f90` and `.i90`

 + Post process HTML links if any other option is possible
 + GitHub FORD project issue [#119](https://github.com/Fortran-FOSS-Programmers/ford/issues/119)
@endtodo

@todo
Enshorten long links in index page:

 + Post process link names with `...` at the end to show that is incomplete ending
 + If the number of characters is  greater than 32 ; then cut at 30 + `...`
 + Issue opened in github project of FORD: [#260](https://github.com/Fortran-FOSS-Programmers/ford/issues/260)
@endtodo

@todo
 Parse file with list of processed tests/drivers and list of reports

@todo
Explore equation numbering with LaTex to MD; Pandoc, Mathjax, FORD interaction

 * Parse file with list of processed tests/drivers and list of reports
@endtodo

@todo 
 Solve FORD incompatiblities for files or use `!FORD_please_ignore_this` tag, that now are excluded such as:

   * `Sources/Lib/Generic`
   * `Sources/Lib/Tools`
   * `mc_tables*`
   * `reference_fe.f90`
@endtodo

