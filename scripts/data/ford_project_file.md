Title: FEMPAR
Project: FEMPAR
Date: 2019
Author: Large Scale Scientific Computing
Autor_description: The Large Scale Scientific Computing group develops novel finite element formulations for solid mechanics and fluid dynamics (turbulent incompressible and compressible flows). It is particularly focused on the scalability of the whole simulation process on the largest supercomputers today. In this sense, it develops novel domain decomposition preconditioners and implementations that are scalable at extreme scales.
Summary: ![fempar_logo](fempar_logo_color.png){width=35%}  
favicon: fempar_logo.png
base_url: http://fempar.org/
github: https://gitlab.com/fempar/fempar
gitter_sidecar: FEMPAR/community
website: http://fempar.org/
blank-value: 
docmark: <
predocmark: >
docmark_alt: ^
predocmark_alt: v
display: public
         protected
         private
search: true
license: gfdl
preprocess: true
source: true
graph: true 
graph_maxdepth: 20
graph_maxnodes: 100
creation_date: <br> %Y-%m-%d %H:%M:%S %Z
print_creation_date: true
extensions: f90
            i90
fpp_extensions: i90
                f90
src_dir: Sources/Lib
         Sources/Lib/Tools/OutputHandler
         Sources/Lib/Tools/Parameters
         Sources/Lib/Tools/std_vector
         Tutorials
exclude_dir: Sources/Lib/Tools
             Sources/Lib/Generic
exclude: mc_tables_quad2trian.i90
         mc_tables_qua4.i90
         mc_tables_hex8.i90
         mc_tables_tri3.i90
         mc_tables_tet4.i90
         reference_fe.f90
include: Sources/Include
         Sources/Lib/Generic
         Sources/
extra_mods: iso_fortran_env:https://gcc.gnu.org/onlinedocs/gfortran/ISO_005fFORTRAN_005fENV.html
            FLAP:http://szaghi.github.io/FLAP
            PENF:http://szaghi.github.io/PENF
            XH5For:https://gitlab.com/fempar/XH5For
            FPL:https://gitlab.com/fempar/FPL
            FortranParser:https://github.com/jacopo-chevallard/FortranParser
md_extensions: markdown.extensions.toc
               markdown.extensions.extra
               markdown.extensions.footnotes


**Finite Element Multiphysics PARallel solvers**

**FEMPAR** is a scientific software for the simulation of problems governed by partial differential equations (PDEs). It provides a set of state-of-the-art numerical discretizations, including finite element methods, discontinuous Galerkin methods, XFEM, and spline-based functional spaces. The library was originally designed to efficiently exploit distributed-memory supercomputers and easily handle multiphysics problems. It also provides a set of highly scalable numerical linear algebra solvers based on multilevel domain decomposition for the systems of equations that arise from PDE discretizations. Some applications of FEMPAR include the simulation of metal additive manufacturing processes, superconductor devices, breeding blankets in fusion reactors, and nuclear waste repositories.

## Software design

For those who are interested on the design and rationale behind the mathematically-supported software abstractions in FEMPAR, a very through presentation is available at the following reference:

Santiago Badia, Alberto F. Martín and Javier Principe. 
FEMPAR: An object-oriented parallel finite element framework. 
*Archives of Computational Methods in Engineering* 25, 2 (2018), 195–271. 
[[ArXiv link]](https://arxiv.org/abs/1708.01773) [[DOI]](https://link.springer.com/article/10.1007%2Fs11831-017-9244-1)

## Links

- [Web page](http://www.fempar.org/)
- [Official repository](https://www.gitlab.com/fempar/fempar)
- [Mirror repository](https://www.github.com/fempar/fempar)
- [Testing dashboard](https://cdash.cimne.upc.edu/user.php)
- [Documentation](http://fempar.gitlab.io/documentation/)
- [Developers mailing list](https://listas.cimne.upc.edu/mailman/listinfo/fempar-dev)

