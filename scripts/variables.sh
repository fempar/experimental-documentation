#!/bin/bash

# Absolute and relative path of this script
rel_path=$(dirname $0)
abs_path=$(dirname $(readlink -f $0))

# All scripts and initial data must be placed in the same directory
scripts_dir=$abs_path
data_dir=$scripts_dir/data
main_pages_dir=$data_dir/MainPages
ford_project_path=$data_dir/ford_project_file.md
fempar_logo_path=$data_dir/fempar_logo.png
fempar_logo_color_path=$data_dir/Fempar_logo_color_2.png

# Temporal directories and files at $TMPDIR
tmp_dir=$(mktemp -d -u)
tmp_pages_dir=$tmp_dir/Pages
tmp_data_dir=$tmp_dir/Data
tmp_readme_page=$tmp_pages_dir/01_Readme.md
# Temporal container pages
tmp_containers_pages_dir=$tmp_pages_dir/02_containers
tmp_containers_index_page=$tmp_containers_pages_dir/index.md
# Temporal tutorial pages
tmp_tutorial_pages_dir=$tmp_pages_dir/03_tutorials
tmp_tutorial_index_page=$tmp_tutorial_pages_dir/index.md
# Temporal core component pages
tmp_core_pages_dir=$tmp_pages_dir/04_core
tmp_core_index_page=$tmp_core_pages_dir/index.md
# Temporal thirdpart pages
tmp_thirdparty_pages_dir=$tmp_pages_dir/05_thirdparty
tmp_thirdparty_index_page=$tmp_thirdparty_pages_dir/index.md

# Fempar repository directories
fempar_sources_dir=$fempar_dir/Sources
fempar_lib_dir=$fempar_sources_dir/Lib
fempar_containers_dir=$fempar_dir/Containers
fempar_tutorials_dir=$fempar_dir/Tutorials
fempar_thirdparty_dir=$fempar_dir/ThirdParty


