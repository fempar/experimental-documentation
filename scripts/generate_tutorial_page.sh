#!/bin/bash

abs_path=$(dirname $(readlink -f $0))
. $abs_path/preprocess_functions.sh

if [ "$#" -ne 4 ]; then
    echo "">&2
    echo "[ERROR] Wrong number of arguments!" >&2
    echo "">&2
    echo "Usage: $0 FEMPAR_TUTORIALS_DIR TUTORIAL_PAGES_DIR TMP_DATA_DIR TUTORIAL_NAME" >&2
    echo "Call: $0 $1 $2 $3 $4" >&2
    echo "">&2
    echo "Input parameters:">&2
    echo "  # \$1 : FEMPAR_TUTORIALS_DIR (Path to FEMPAR tutorials directory)">&2
    echo "  # \$2 : TUTORIAL_PAGES_DIR (Path to directory of temporal page tree in MarkDown)">&2
    echo "  # \$3 : TMP_DATA_DIR (Path to directory of temporal data)">&2
    echo "  # \$4 : TUTORIAL_NAME (Tutorial name)">&2
    exit 1
fi

# Input parameters
fempar_tutorials_dir=$1
tmp_pages_dir=$2
tmp_data_dir=$3
tutorial_name=$4

# Path variables depending on tutorial_name
tutorial_dir=$fempar_tutorials_dir/$tutorial_name
output_dir=$tmp_pages_dir/$tutorial_name
data_dir=$tmp_data_dir/tutorials/$tutorial_name
f90_file_name=$tutorial_dir/$tutorial_name'.f90'

# Create tutorial pages and data work directories
mkdir -p $output_dir
mkdir -p $data_dir

# Copy tutorial folder contain to data_dir
cp -r $tutorial_dir/* $data_dir

## Import documentation from tutorial
## Improvements : Place the documentation in another directory to avoid warnings
##                Search for *.f90 files within the Tutorial directory (Sources/Tutorials/$tutorial_name) ?

read -r -d '' tutorial_content << EOM

[TOC]

## Introduction
{!$data_dir/README.md!}

## The commented code
{!$data_dir/${tutorial_name}_commented_code.text!}

## The plain code
### Program
{!$data_dir/${tutorial_name}_plain_code.text!}
### Modules
{!$data_dir/${tutorial_name}_modules_plain_code.text!}

EOM

write_page "$output_dir/index.md" "$tutorial_name" "Large Scale Scientific Computing" "$tutorial_content"

if [ -d "$tutorial_dir/media" ]; then
    cp -rf $tutorial_dir/media $output_dir
fi

f90_list=$( find $tutorial_dir -type f | sed 's# ##g' | grep f90 | tr '\n' '#' | sed 's/#$//' ) 
num_f90_files=$( echo $f90_list |  sed 's/#/\n/g' | wc -l )
f90_id=1

while [ $f90_id -le $num_f90_files ]
do 
    f90_file_name=$( echo $f90_list | cut -d# -f$f90_id ) 
    $abs_path/fortran2markdown.sh $f90_file_name $data_dir
    let f90_id=f90_id+1
done
