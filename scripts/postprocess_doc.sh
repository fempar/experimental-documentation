#!/bin/bash
#
#     postprocess.sh $1 
# 
# Input:
# 
#  $1: Path to the output html directory

abs_path=`dirname $(readlink -f $0)`
rel_path=`dirname $0`
tmp_dir=$(readlink -f $1)
tmp_tutorial_pages_dir=$(readlink -f $2)
output_dir=$(readlink -f $3)
tutorial_pages_dir=$output_dir/page/03_tutorials

indexFile=$output_dir/index.html


## Print script execution
echo 'Postprocessing FORD output...'
## Change the source icon from GitHub to GitLab
sed -i 's#</title>#</title>\n    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">#' $indexFile
sed -i 's/fa fa-github/fab fa-gitlab/' $indexFile 

# move header logo
cp $tmp_dir/Fempar_logo_color_2.png $output_dir/fempar_logo_color.png

# move tutorial media to production webpage
media_list=$(find $tmp_tutorial_pages_dir -type d -name media -printf "%P\n"  |  tr '\n' '#')
num_media=$( echo $media_list | tr -cd '#' | wc -c )
media_id=1
while [ $media_id -le $num_media ]
do 
    media_path=$(echo $media_list | cut -d# -f$media_id)
    cp -rf $tmp_tutorial_pages_dir/$media_path $tutorial_pages_dir/$media_path
    let media_id=media_id+1
done



