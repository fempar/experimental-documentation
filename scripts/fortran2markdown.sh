#!/bin/bash

if [ "$#" -ne 2 ]; then
  echo "">&2
  echo "[ERROR] Wrong number of arguments!" >&2
  echo "">&2
  echo "Usage: $0 SOURCE_FILE OUTPUT_DIR" >&2
  echo "">&2
  echo "Input parameters:">&2
  echo "  # \$1 : SOURCE_FILE (Path to source .f90 file)">&2
  echo "  # \$2 : OUTPUT_DIR (Path to output directory where to place *_commented_code.text and *_plain_code.text source files )">&2
  exit 1
fi

source_file_path=$(readlink -f $1) 
output_dir=$(readlink -f $2)

if [ ! -f $source_file_path ]; then
  echo "[ERROR] File not found!">&2
  echo "File: $source_file_path">&2
  exit 1
fi

commented_code_path=$output_dir/$(basename $source_file_path   | sed 's/.f90/_commented_code.text/' )
plain_code_path=$output_dir/$(basename $source_file_path | sed 's/.f90/_plain_code.text/' )

echo "Preprocessing F90 to Markdown for: $source_file_path"

## Filter comments, keep only !*, ignore ! and !**
output_path=$commented_code_path
echo "  Writing data: $output_path" 
sed 's/^[[:space:]]*!\*/\*\*\*/g' $source_file_path | grep -v '^[[:space:]]*!' | grep -v '^[[:space:]]*\*\*\*\*' >> $output_path

doc_lines=$(sed -n '/^\*\*\*/=' $output_path | sed -z 's/\n/#/g' | sed 's/#$//' )
num_lines=$(cat $output_path | wc -l )

id_line=1
num_doc_lines=$(echo $doc_lines | sed 's/#/\n/g' | wc -l)

if  [[ -z $doc_lines  ]]
then
  ## If no documentation( or only page title page): remove code and jump to 'Plain Code' section
  echo '' > $output_path 
else

  n_insert=1
  while [ $id_line -le $num_doc_lines ]
  do
    line=$(echo $doc_lines | cut -f$id_line -d#)
   
    if [ $id_line -ne 1 ] 
    then
        if [  $(($line-$previous_line)) -gt 1 ] 
        then      
            command="sed -i '$(( $previous_line + $n_insert -1 )) a \\\\n\`\`\` fortran' $output_path"
            eval $command
            command="sed -i '$(( $line + $n_insert  )) a \`\`\`' $output_path"
            eval $command
            let n_insert=n_insert+3
        fi
    fi        
    previous_line=$line
    let id_line=id_line+1
  done

  if [ $previous_line -ne $num_lines ]
  then
    line=$num_lines
    command="sed -i '$(( $previous_line + $n_insert -1 )) a \\\\n\`\`\` fortran' $output_path"
    eval $command
    command="sed -i '$(( $line + $n_insert +1  )) a \`\`\`' $output_path"
    eval $command
  fi

  sed -i 's/^\*\*\*[[:space:]]*//g' $output_path
 

fi

## Generate Plain Code page, to append at the end as in deal.ii tutorials
output_path=$plain_code_path
if [ TRUE ] 
then 
    echo "  Writing data: $output_path" 
    echo '```fortran'                                >> $output_path
    cat $source_file_path | grep -v '^[[:space:]]*!' >> $output_path
    echo '```'                                       >> $output_path
fi

