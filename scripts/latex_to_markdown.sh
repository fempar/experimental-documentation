#!/bin/bash
abs_path=`dirname $(readlink -f $0)`
script_name=`basename $0`
#
# To write this script we used:
# https://stackoverflow.com/questions/402377/using-getopts-in-bash-shell-script-to-get-long-and-short-command-line-options
#
# We assume compilers and variables can be found by cmake, i.e.
# that approrpriate environment variables have been set (we suggest
# using environment modules). If this is not the case, it can be
# done here, e.g., modifying these lines:
#source /opt/intel/bin/compilervars.sh intel64
#export HDF5_ROOT=/opt/hdf5/1.10.0/GNU/6.2.0/openmpi/1.8.4
#export MKLROOT=/opt/intel/mkl
#
usage()
{
cat << EOF
usage: $script_name  [ OPTIONS ] 

This script transforms LaTEX files to MarkDown, Pandoc (https://pandoc.org/) must be installed

OPTIONS:
   -h --help 
        Show this message
   -s --source 
        LaTEX file (.tex) to be converted
   -b --bibliography
        (NOT REQUIRED) References file (.bib) containing the LaTEX references.
   -o --output
        (NOT REQUIRED) Output file (.md)  
EOF
}
# NOTE: This requires GNU getopt.
TEMP=`getopt -o hs:b:o: --long help,source:,biblography:,output: -n '$script_name' -- "$@"`
if [ $? != 0 ] ; then usage >&2 ; exit 1 ; fi

# Note the quotes around `$TEMP': they are essential!
#echo "$TEMP"
eval set -- "$TEMP"

source_flag_present=0
bib_flag_present=0
while true; do
    case "$1" in
	-h | --help )     usage; exit 1;;
	-s | --source ) 
            source_flag_present=1
            source_file=$2
            shift 2;;
	-b | --bibliography ) 
            bib_flag_present=1
            references_file=$2
            shift 2;;
	-o | --output ) 
            output_flag_present=1
            output_file=$2
            shift 2;;
	-- ) shift; break ;;
	* ) break ;;
    esac
done


if [[  $source_flag_present -eq 0 ]]
then 
    echo " Error: Source file must be present "
    usage
    exit
fi

if [[ $source_file != *.tex ]]
then 
  echo " Error: Input file is not a LaTex file (.tex) "
  usage
  exit 1
fi

which pandoc > /dev/null
if [ $? -ne 0 ]
then
    echo ' Error: PANDOC package not installed, LaTEX file wont be processed'
    usage
    exit
fi
 
if [[ $output_flag_present -eq 0 ]]
then 
  output_dir=`pwd`    #`dirname $(readlink -f $source_file)`
  file_name=`basename $(readlink -f $source_file) | sed 's:.tex:.md:'`
  output_file=$output_dir'/'$file_name
else
  if [[ $output_file != *.md ]]
  then
    echo " Error: Output file is not a MarkDown file (.md) "
    usage
    exit 1
  fi
fi
 
if [[ $bib_flag_present -eq 1 ]]
then 
  if [[ $references_file != *.bib ]]
  then
    echo " Error: References file is not a BibTEX file (.bib) "
    usage
    exit
  fi

  tmp_md=$output_dir/tmp.md

  pandoc -f latex -t markdown --standalone --filter pandoc-eqnos --biblatex --bibliography=$references_file --metadata=link-citations:true --atx-headers $source_file -o $tmp_md
  echo '# References ' >> $tmp_md
  pandoc -f markdown -t markdown_mmd-mmd_link_attributes+link_attributes --filter pandoc-eqnos --filter pandoc-citeproc --atx-headers  $tmp_md -o $output_file
  sed -i 's/\[references\]//g'  $output_file
  rm $tmp_md

else 

  pandoc -f latex -t markdown_mmd-mmd_link_attributes+link_attributes --standalone --filter pandoc-eqnos --atx-headers $source_file -o $output_file

fi


##### Fix equation management

## Store implicit \$ as IMPLICIT_DOLLAR_TEMP
sed -i 's/\\\$/IMPLICIT_DOLLAR_TEMP/g' $output_file

## $$ ... $$ to \begin{equation}  \end{equation} 
while [ $(sed -n  '/\$\$/p' $output_file | wc -w ) -gt 0 ]
do
    sed -i '0,/\$\$/{s/\$\$/\\begin\{equation\}/}' $output_file
    sed -i '0,/\$\$/{s/\$\$/\\end\{equation\}/}' $output_file
done
 
## $ ... $ to \( ... \)
while [ $(sed -n  '/\$/p' $output_file  | wc -w) -gt 0 ]
do
    sed -i '0,/\$/{s/\$/\\(/}' $output_file
    sed -i '0,/\$/{s/\$/\\)/}' $output_file
done

## Restore implicit IMPLICIT_DOLLAR_TEMP as \$ 
sed -i 's/IMPLICIT_DOLLAR_TEMP/\\\$/g' $output_file
