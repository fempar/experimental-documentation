#!/bin/bash

#####################################################################
# This shell script orchestrates the workflow to create fempar documentation
# based on ford and also some custom pages for tutorials.
# Is assumed that this scripts is located inside a fempar git repository.
#####################################################################

if [ "$#" -ne 2 ]; then
    echo "">&2
    echo "[ERROR] Wrong number of arguments! (2 expected)" >&2
    echo "">&2
    echo "Usage: $0 FEMPAR_DIR OUTPUT_DIR" >&2
    echo "Call: $0 $1 $2 $3 $4" >&2
    echo "">&2
    echo "Input parameters:">&2
    echo "  # \$1 : FEMPAR_DIR (Path to FEMPAR repository)">&2
    echo "  # \$2 : OUTPUT_DIR (Output path to store generated documentation)">&2
    exit 1
fi


if [ ! -x "$(command -v ford)" ]; then 
    echo "[ERROR] `ford` not found!" >&2
    echo "`ford` documentation generator does not exists or is not located in \$PATH environment variable!" >&2
    exit 1
fi

# Absolute and relative path of this script
rel_path=$(dirname $0)
abs_path=$(dirname $(readlink -f $0))

# Input arguments
fempar_dir=$(realpath $1)
output_dir=$(realpath $2)
echo $fempar_dir
echo $output_dir

# Load environment variables
. $abs_path/variables.sh

# Load preprocess functions
. $scripts_dir/preprocess_functions.sh

#####################################################################
# Preprocess source code to generage markdown files and use existing 
# markdown files to generage pages. 
#####################################################################
# Create temporal pages structure 
mkdir -p $tmp_dir $tmp_pages_dir
mkdir -p $tmp_tutorial_pages_dir $tmp_core_pages_dir $tmp_thirdparty_pages_dir
# Copy templates to tmp directory and create Pages hierarchy
cp $main_pages_dir/*.* $tmp_pages_dir
# Create main Readme page
write_page "$tmp_readme_page" "Readme" "Large Scale Scientific Computing" "{!$fempar_dir/README.md!}"
# Create Pages for container components from *.md files at fempar/Containers/*
create_reference_pages_recursive "$fempar_containers_dir" "$tmp_containers_pages_dir" "README.md" "2" "Containers" 
# Create Pages for core components from *.md files at fempar/Sources/Lib/*
create_reference_pages_recursive "$fempar_lib_dir" "$tmp_core_pages_dir" "*.md" "99" "Core" 
# Create Pages for thirdparty components from README.md files at fempar/ThirParty/*
create_reference_pages_recursive "$fempar_thirdparty_dir" "$tmp_thirdparty_pages_dir" "README.md" "2" "Thirdparty" 
# Preprocess *.f90 tutorial files and create Tutorial pages
create_tutorial_pages_from_source $fempar_tutorials_dir $tmp_tutorial_pages_dir

#####################################################################
# Fortran source code automatic documentation
#####################################################################
(cd $fempar_dir && cp $ford_project_path ford.md && cp $fempar_logo_path fempar_logo.png && cp $fempar_logo_color_path $tmp_dir && ford ford.md -s $data_dir/style.css -p $tmp_pages_dir -o $output_dir && rm ford.md fempar_logo.png)

#####################################################################
# Postprocess ford generated documentation
#####################################################################
$scripts_dir/postprocess_doc.sh $tmp_dir $tmp_tutorial_pages_dir $output_dir

rm -r $tmp_dir 
