# Fempar project documentation

[Fempar](https://gitlab.com/fempar/fempar) documentation can be created by means of the scripts provided in this repository.


## Requirements
 - `ford` executable must be located in any directory of the `$PATH` environment variable.
 - Fempar sources must be located in any local directory (`$FEMPAR_DIR`).
 - Output directory must be writable (`$OUTPUT_DIR`)

## Generating documentation

```
$ ./scripts/create_doc.sh $FEMPAR_DIR $OUTPUT_DIR
```
